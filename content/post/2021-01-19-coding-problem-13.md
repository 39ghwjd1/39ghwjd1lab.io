---
title: SSAFY Problem Solving 13
subtitle: 
date: 2021-01-19
tags: ["code"]
type: post
---

알고리즘 문제 풀이 13일차 및 JS 개념 정리 마무리  

- Tree-coding repo에 관련 정보 정리해서 넣을 듯 => 하나 공부하면서 직접 번역해서 추가함 
    - 관련 링크들을 찾아 더 딥하게 들어가야 함 

<!--more-->

- 백준 11721

    - 그렇게 어려운 문제가 아님 

    - substring을 활용 

{{< highlight java "linenos=inline">}}
import java.util.*;

public class Main{

     public static void main(String []args){
        Scanner input = new Scanner(System.in);
        
        String a = input.next();
        
        for(int i = 0 ; i < a.length() ; i += 10){
            
            if(i+10 >= a.length()){
                System.out.println(a.substring(i,a.length()));
            }
            else{
                System.out.println(a.substring(i,i+10));
                
            } 
        }
       
     }
}
{{</ highlight >}}
```