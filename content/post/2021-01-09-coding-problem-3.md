---
title: SSAFY Problem Solving 3
subtitle: 
date: 2021-01-09
tags: ["code"]
type: post
---

알고리즘 문제 풀이 3일차 

<!--more-->

- Java String regex => Tag Content Extracter 풀이 

    - 태그를 확인해서 옳은 태그 일시 추출하는 문제 

    - 내부 태그가 있다면 => "^<+" 기호를 통해 내부 안에 태그가 없을 때까지 검사 

    - https://www.hackerrank.com/challenges/tag-content-extractor/problem?h_r=next-challenge&h_v=
    
    - https://www.hackerrank.com/rest/contests/master/challenges/tag-content-extractor/hackers/RWCoon/download_solution

{{< highlight java "linenos=inline">}}
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution{
	public static void main(String[] args){
		
		Scanner in = new Scanner(System.in);
		int testCases = Integer.parseInt(in.nextLine());
        String regex = "<(.+)>([^<]+)</\\1>";
		Pattern p = Pattern.compile(regex);
            
    
        while(testCases>0){
			String line = in.nextLine();
			
          	//Write your code here
            Matcher m = p.matcher(line);
        
            if(m.find()){
                do{
                    System.out.println(m.group(2));
                }
                while(m.find());
            }
            else {
                System.out.println("None");
            }
            
			testCases--;
		}
	}
}

{{</ highlight >}}

- 30 Days of Code 

    - 제네릭에 대한 알고리즘 문제 

    - 기본 문제 => 기본에 충실하자 

    - https://www.hackerrank.com/challenges/30-generics/problem

{{< highlight java "linenos=inline">}}
public void printArray(T[] array){
        for(T k : array){
            System.out.println(k);
        }
    }
{{</ highlight >}}

- Hackerrank Java

    - Annotation : 어렵다 => 이 부분도 추가 공부가 필요할 듯 

    - https://www.hackerrank.com/challenges/java-annotations/problem

{{< highlight java "linenos=inline">}}
import java.lang.annotation.*;
import java.lang.reflect.*;
import java.util.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@interface FamilyBudget {
	String userRole() default "GUEST";
	int budgetLimit() default 50;
}

class FamilyMember {
	
    
    @FamilyBudget(userRole="SENIOR",budgetLimit=100)
	public void seniorMember(int budget, int moneySpend) {
		System.out.println("Senior Member");
		System.out.println("Spend: " + moneySpend);
		System.out.println("Budget Left: " + (budget - moneySpend));
	}

	@FamilyBudget(userRole="JUNIOR", budgetLimit=50)
	public void juniorUser(int budget, int moneySpend) {
		System.out.println("Junior Member");
		System.out.println("Spend: " + moneySpend);
		System.out.println("Budget Left: " + (budget - moneySpend));
	}
}

public class Solution {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int testCases = Integer.parseInt(in.nextLine());
		while (testCases > 0) {
			String role = in.next();
			int spend = in.nextInt();
			try {
				Class annotatedClass = FamilyMember.class;
				Method[] methods = annotatedClass.getMethods();
				for (Method method : methods) {
					if (method.isAnnotationPresent(FamilyBudget.class)) {
						FamilyBudget family = method
								.getAnnotation(FamilyBudget.class);
						String userRole = family.userRole();
						int budgetLimit = family.budgetLimit();
						if (userRole.equals(role)) {
							if(spend<=budgetLimit){
								method.invoke(FamilyMember.class.newInstance(),
										budgetLimit, spend);
							}else{
								System.out.println("Budget Limit Over");
							}
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			testCases--;
		}
	}
}

{{</ highlight >}}

- Hackerrank Java 

    - covariant 

    - https://www.hackerrank.com/challenges/java-covariance/problem?h_r=next-challenge&h_v=zen

    - http://wiki.c2.com/?CovariantReturnTypes

{{< highlight java "linenos=inline">}}
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

//Complete the classes below
class Flower {
    String whatsYourName(){
        return "I have many names and types";
    }
}

class Jasmine extends Flower {
    String whatsYourName(){
        return "Jasmine";
    }
}

class Lily extends Flower{
    String whatsYourName(){
        return "Lily";
    }
}

class Region {
    Flower yourNationalFlower(){
        return new Flower();
    }
}

class WestBengal extends Region {
    Jasmine yourNationalFlower(){
        return new Jasmine();
    }
}

class AndhraPradesh extends Region {
    Lily yourNationalFlower(){
        return new Lily();
    }
}


public class Solution {
  public static void main(String[] args) throws IOException {
      BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
      String s = reader.readLine().trim();
      Region region = null;
      switch (s) {
        case "WestBengal":
          region = new WestBengal();
          break;
        case "AndhraPradesh":
          region = new AndhraPradesh();
          break;
      }
      Flower flower = region.yourNationalFlower();
      System.out.println(flower.whatsYourName());
    }
}
{{</ highlight >}}


- Java Lambda Expression

    - Hackerrank 문제 

    - https://www.hackerrank.com/challenges/java-lambda-expressions/problem

    - https://docs.oracle.com/javase/tutorial/java/javaOO/lambdaexpressions.html#approach1



{{< highlight java "linenos=inline">}}
import java.io.*;
import java.util.*;
interface PerformOperation {
 boolean check(int a);
}
class MyMath {
 public static boolean checker(PerformOperation p, int num) {
  return p.check(num);
 }

   // Write your code here
    PerformOperation isOdd() {
        return (n)->{
            if(n % 2 == 0){
                return false;
            }
            return true;
        };
    }
    
    PerformOperation isPrime() {
        return (n)->{
           return java.math.BigInteger.valueOf(n).isProbablePrime(15) ? true : false;
        };
    }
    
    PerformOperation isPalindrome() {
        return (n)->{
            String f = String.valueOf(n);
            for(int i = 0 ; i < f.length()/2 ; i++){
                if(f.charAt(i) != f.charAt(f.length()-i-1)){
                    return false;
                }
            }
            return true;
        };
    }
}

public class Solution {

 public static void main(String[] args) throws IOException {
  MyMath ob = new MyMath();
  BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
  int T = Integer.parseInt(br.readLine());
  PerformOperation op;
  boolean ret = false;
  String ans = null;
  while (T--> 0) {
   String s = br.readLine().trim();
   StringTokenizer st = new StringTokenizer(s);
   int ch = Integer.parseInt(st.nextToken());
   int num = Integer.parseInt(st.nextToken());
   if (ch == 1) {
    op = ob.isOdd();
    ret = ob.checker(op, num);
    ans = (ret) ? "ODD" : "EVEN";
   } else if (ch == 2) {
    op = ob.isPrime();
    ret = ob.checker(op, num);
    ans = (ret) ? "PRIME" : "COMPOSITE";
   } else if (ch == 3) {
    op = ob.isPalindrome();
    ret = ob.checker(op, num);
    ans = (ret) ? "PALINDROME" : "NOT PALINDROME";

   }
   System.out.println(ans);
  }
 }
}
{{</ highlight >}}
