---
title: SSAFY Problem Solving 7
subtitle: 
date: 2021-01-13
tags: ["code"]
type: post
---

알고리즘 문제 풀이 7일차 

<!--more-->

- Hackerrank 30 Days of Code 

    - 시간 복잡도를 고려한 소수 계산하기 

    - https://www.hackerrank.com/challenges/30-running-time-and-complexity/problem

{{< highlight java "linenos=inline">}}
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        /* Enter your code here. */
        Scanner input = new Scanner(System.in);
        int cases = input.nextInt();
        for(int i = 0 ; i< cases ; i++){
            int k1 = input.nextInt();
            if(k1 ==1){
                System.out.println("Not prime");
                continue;
            }
            boolean check = false;
            for(int k = 2; k <= Math.sqrt(k1) ; k++){
                if(k1 % k == 0){
                    check = true;
                    break;
                }
            } 
            if(check){
                System.out.println("Not prime");
            }
            else {
                System.out.println("Prime");
            }
        }
    }
}

{{</ highlight >}}

- 백준 2775

    - https://www.acmicpc.net/problem/2775

    - 층의 합을 구하면 되는 문제인데 약간 어렵게 푼듯?

```java
import java.util.*;

public class Main{
    
    public static void main(String... args){
        Scanner input = new Scanner(System.in);
        int cases = input.nextInt();
        for(int i = 0 ; i < cases ; i++){
            int test = input.nextInt();
            int test1 = input.nextInt();
            int[][] test2 = new int[test+1][test1];
            // int result = 0;
            for(int j = 0; j <= test ; j++){
                
                for(int k = 1; k <= test1 ; k++){
                    //  result1 += k;
                    if(j == 0){
                        test2[j][k-1] = k;   
                    }
                    else{
                        int result1 = 0;
                        for(int k1 = 0 ; k1 < k; k1++){
                            result1+= test2[j-1][k1];
                        }
                        test2[j][k-1] = result1;
                    }
                     
                }
                // test2[j+1][test1-1] = result1;
            }
            System.out.println(test2[test][test1-1]);
            // System.out.println(Arrays.deepToString(test2));
            
        }
    }   
}
```




- 백준 부분 문제 해결 

    - https://www.acmicpc.net/problem/17225

    - 우선순위 큐를 이용해서 원하는 대로 정렬 후 문제를 해결함 

```java
import java.util.*;
import java.io.*;

public class test{

     public static void main(String []args){
        
        try{
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 
            
            String[] test = br.readLine().split(" ");
            
            int sangmin = Integer.parseInt(test[0]);
            
            int jisu = Integer.parseInt(test[1]);
            
            int client = Integer.parseInt(test[2]);
            
            PriorityQueue<String> queue1 = new PriorityQueue<String>((x,y)->{

                int val1 = Integer.parseInt(x.split("-")[0]);
                int val2 = Integer.parseInt((y.split("-")[0]));

                if(val1 == val2){
                    return x.split("-")[1].compareTo(y.split("-")[1]);
                }
                return val1-val2;

            });
            int sangmincount = 0;
            int jisucount = 0;
            for(int i = 0 ; i < client;i++){
                
                String[] check = br.readLine().split(" ");
                
                int ordertime = Integer.parseInt(check[0]);
                
                String color = check[1];

                int parentCount = Integer.parseInt(check[2]);
                
                // System.out.println(ordertime + "," +color + "," + parentCount);
                
                if(color.equals("B")){
                    queue1.add(ordertime+"-B");
                    sangmincount++;
                    for(int j = 0 ; j < parentCount-1 ; j++){
                        ordertime = ordertime+sangmin;
                        queue1.add(ordertime+"-B");
                        sangmincount++;
                    }
                }
                else if(color.equals("R")){
                    queue1.add(ordertime+"-R");
                    jisucount++;
                    for(int j = 0 ; j < parentCount-1 ; j++){
                        ordertime = ordertime+jisu;
                        queue1.add(ordertime+"-R");
                        jisucount++;
                    }
                }
            
                // if(color.equals("B")){
                //     queue.add();
                // }
                // else if(color.equals("R")){
                    
                // }
                
            }
            br.close();
            
            
            int count = 1;
            // System.out.println(sangmincount+" " +jisuconut);
            ArrayList<Integer> arr1 = new ArrayList<Integer>();
            ArrayList<Integer> arr2 = new ArrayList<Integer>();
            while(queue1.size() != 0){
                String checking = queue1.poll();
                if(checking.contains("B")){
                    arr1.add(count);
                }
                else {
                    arr2.add(count);
                }
                count++;
                
            }
            System.out.println(sangmincount);
            for(int k : arr1){
                System.out.print(k+" ");
            }
            System.out.println();
            System.out.println(jisucount);
            for(int k : arr2){
                System.out.print(k+" ");
            }
        
        
        }
        catch(IOException e){
            
        }

     }
}
```


