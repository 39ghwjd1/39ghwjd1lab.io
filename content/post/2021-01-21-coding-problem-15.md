---
title: SSAFY Problem Solving 15
subtitle: 
date: 2021-01-21
tags: ["code"]
type: post
---

알고리즘 문제 풀이 15일차   

- Hackerrank DB 문제 풀이 하며 공부

    - MS SQL Index

        - https://www.hackerrank.com/challenges/indexes-2/problem

        - https://www.hackerrank.com/challenges/indexes-3/problem

        - https://www.hackerrank.com/challenges/indexes-4/problem

    - https://www.hackerrank.com/challenges/ctci-bubble-sort/problem?h_l=interview&playlist_slugs%5B%5D=nutanix

        - Bubble Sort 구현

<!--more-->

- Sorting: Bubble Sort 

{{< highlight java "linenos=inline">}}
static void countSwaps(int[] a) {
        int count = 0;
        for(int i = 0 ; i < a.length ; i++){
            for(int j = 0 ; j < a.length-1; j++){
                if(a[j] > a[j+1]){
                    int temp = a[j];
                    a[j] = a[j+1];
                    a[j+1] = temp;
                    count++;
                }
            }
        }
        System.out.println("Array is sorted in " + count + " swaps.");
        System.out.println("First Element: " + a[0]);
        System.out.println("Last Element: "+a[a.length-1]);

    }
{{</ highlight >}}
```