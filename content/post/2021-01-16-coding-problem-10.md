---
title: SSAFY Problem Solving 10
subtitle: 
date: 2021-01-16
tags: ["code"]
type: post
---

알고리즘 문제 풀이 10일차 

<!--more-->

- Hackerrank 30 Days of Code 

    - Day 27 RegEx, Patterns, and Intro to Databases 

        - 조건에 따른 여러 경우를 생각하면 됨 

    - https://www.hackerrank.com/challenges/30-regex-patterns/problem

    - https://developer.mozilla.org/ko/docs/Web/JavaScript/Reference/Global_Objects/RegExp/test

    

{{< highlight java "linenos=inline">}}
import java.util.*;

public class Solution {

    public static int minimum_index(int[] seq) {
        if (seq.length == 0) {
            throw new IllegalArgumentException("Cannot get the minimum value index from an empty sequence");
        }
        int min_idx = 0;
        for (int i = 1; i < seq.length; ++i) {
            if (seq[i] < seq[min_idx]) {
                min_idx = i;
            }
        }
        return min_idx;
    }

    static class TestDataEmptyArray {
        public static int[] get_array() {
            // complete this function
            int[] arr = {};
            return arr;
        }
    }

    static class TestDataUniqueValues {
        public static int[] get_array() {
            // complete this function
            int[] test = new int[]{1,2};

            return test;
        }

        public static int get_expected_result() {
            // complete this function
            // int[] test = new int[]{1,2};
            return 0;
        }
    }

    static class TestDataExactlyTwoDifferentMinimums {
        public static int[] get_array() {
            // complete this function
            int[] test = new int[]{0,0};
            return test;
        }

        public static int get_expected_result() {
            // complete this function
            return 0;
        }
    }

    
	public static void TestWithEmptyArray() {
        try {
            int[] seq = TestDataEmptyArray.get_array();
            int result = minimum_index(seq);
        } catch (IllegalArgumentException e) {
            return;
        }
        throw new AssertionError("Exception wasn't thrown as expected");
    }

    public static void TestWithUniqueValues() {
        int[] seq = TestDataUniqueValues.get_array();
        if (seq.length < 2) {
            throw new AssertionError("less than 2 elements in the array");
        }

        Integer[] tmp = new Integer[seq.length];
        for (int i = 0; i < seq.length; ++i) {
            tmp[i] = Integer.valueOf(seq[i]);
        }
        if (!((new LinkedHashSet<Integer>(Arrays.asList(tmp))).size() == seq.length)) {
            throw new AssertionError("not all values are unique");
        }

        int expected_result = TestDataUniqueValues.get_expected_result();
        int result = minimum_index(seq);
        if (result != expected_result) {
            throw new AssertionError("result is different than the expected result");
        }
    }

    public static void TestWithExactlyTwoDifferentMinimums() {
        int[] seq = TestDataExactlyTwoDifferentMinimums.get_array();
        if (seq.length < 2) {
            throw new AssertionError("less than 2 elements in the array");
        }

        int[] tmp = seq.clone();
        Arrays.sort(tmp);
        if (!(tmp[0] == tmp[1] && (tmp.length == 2 || tmp[1] < tmp[2]))) {
            throw new AssertionError("there are not exactly two minimums in the array");
        }

        int expected_result = TestDataExactlyTwoDifferentMinimums.get_expected_result();
        int result = minimum_index(seq);
        if (result != expected_result) {
            throw new AssertionError("result is different than the expected result");
        }
    }

    public static void main(String[] args) {
        TestWithEmptyArray();
        TestWithUniqueValues();
        TestWithExactlyTwoDifferentMinimums();
        System.out.println("OK");
    }
}
{{</ highlight >}}

- baekjoon 2331

    - https://www.acmicpc.net/problem/2331

    - 간단한 문제를 자바를 써서 하니, 복잡하게 푸는 경우가 생김 

```java
import java.util.*;

public class Main{

     public static void main(String []args){
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        int p = input.nextInt();
        ArrayList<String> arraylist = new ArrayList<String>();
        arraylist.add("0");
        arraylist.add(a+"");
        
        //String check = "-1";
        int count = 1;
        int found = 0;
        while(true){
            String[] getting = arraylist.get(count).split("");
            int result = 0; 
            for(int i = 0; i < getting.length ;i++){
                int result1 = 1;
                for(int j = 0 ; j < p ; j++){
                    
                    result1 *= Integer.parseInt(getting[i]);
                }
                result+=result1;
            }
            int check = arraylist.indexOf(result+"");
            if(check!=-1){
                found = check;
                break;
            }
            else {
                // check = result+"";
                arraylist.add(result+"");
                count++;

            }
            // System.out.print(result+" ");
            // if(count==15){
            //     break;
            // }
        }
        
        System.out.println(found-1);
        
        
        
     }
}
```