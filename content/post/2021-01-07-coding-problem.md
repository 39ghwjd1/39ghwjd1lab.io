---
title: SSAFY Problem Solving 1
subtitle: 
date: 2021-01-07
tags: ["code"]
type: post
---

오늘부터 몇 개씩 하루에 풀어서 올릴 예정

알고리즘 사이트 4개 정도 선정

<!--more-->

- 평균 구하기 문제

    - https://programmers.co.kr/learn/courses/30/lessons/12944?language=javascript

    - reduce 함수를 이용해서 풀면 쉽게 가능 

```javascript
    function solution(arr) {
    var answer = 0;
    
    let sum = arr.reduce((sum, arr)=>{
        sum = sum + arr;
        return sum;
    }, 0);
    
    return sum / arr.length
    
}
```

- 3진법 뒤집기 문제

    - https://programmers.co.kr/learn/courses/30/lessons/68935

    - https://ithub.tistory.com/290

    - https://developer.mozilla.org/ko/docs/Web/JavaScript/Reference/Global_Objects/Number/toString

    - https://developer.mozilla.org/ko/docs/Web/JavaScript/Reference/Global_Objects/Array/reverse

    - js에서는 자료형이 명확하지 않기 때문에,
        - parseInt, toString 활용에 주의할 것 

{{< highlight javascript "linenos=inline">}}
    
    function solution(n) {
        let answer3 = n.toString(3);
        
        let answerSplit = answer3.split('').reverse().join('');
        
        let answer = parseInt(parseInt(answerSplit,3).toString(10));
        
        return answer;
    }

{{</ highlight >}}

- Hackerrank
    
    - Day 16 : Interface   
        - 인수들의 합을 구하는 문제 
        - https://www.hackerrank.com/challenges/30-interfaces/problem

{{< highlight java "linenos=inline">}}
    
    public int divisorSum(int n) {
        
        int result = 0;
        for(int i = 1 ; i <= Math.sqrt(n); i++){
            if(n % i == 0){
                if(i == Math.sqrt(n)){
                    result += i;
                }
                else {
                    result += i;
                    result += n / i;
                }    
            }
        }
        
        return result;
    }

{{</ highlight >}}

- Regex : 정규 표현식 
    - 정규 표현식에 익숙해져라 
    - https://developer.mozilla.org/ko/docs/Web/JavaScript/Guide/%EC%A0%95%EA%B7%9C%EC%8B%9D
        - ? 부분 참고 
    - https://www.regextutorial.org/regex-for-numbers-and-ranges.php

    - https://coding-factory.tistory.com/529

{{< highlight java "linenos=inline">}}
    import java.util.regex.Matcher;
    import java.util.regex.Pattern;
    import java.util.Scanner;

    class Solution{

        public static void main(String[] args){
            Scanner in = new Scanner(System.in);
            while(in.hasNext()){
                String IP = in.next();
                System.out.println(IP.matches(new MyRegex().pattern));
            }
        }
    }

    //Write your code here
    class MyRegex{
        
        public String pattern;
        public MyRegex(){
                    
            this.pattern = "^([01]?[0-9][0-9]?|2[0-4][0-9]|25[0-5])\\.([01]?[0-9][0-9]?|2[0-4][0-9]|25[0-5])\\.([01]?[0-9][0-9]?|2[0-4][0-9]|25[0-5])\\.([01]?[0-9][0-9]?|2[0-4][0-9]|25[0-5])$";
                
        }    
    }

{{</ highlight >}}
