---
title: SSAFY Problem Solving 6 
subtitle: 
date: 2021-01-12
tags: ["code"]
type: post
---

알고리즘 문제 풀이 6일차 

<!--more-->

- More Linked List => removeDuplicatedNodes 메소드 구현 

    - Hackerrank 30 Days Of Code 

    - 연결 리스트의 중복 값을 가지는 노드들을 삭제하라 
    
    - 시작 노드를 기준을 두고 next 연산자를 통해 다른 노드들로 넘어가면서 
        - data 값을 비교하면서 자신을 제외한 다른 중복 노드의 연결 상태를 끊어준다. 
    
    - data 값이 다르다면, 그 노드로 포인터를 넘겨준다.

{{< highlight java "linenos=inline">}}
import java.io.*;
import java.util.*;
class Node{
	int data;
	Node next;
	Node(int d){
        data=d;
        next=null;
    }
	
}
class Solution
{

    public static Node removeDuplicates(Node head) {
      //Write your code here
      Node test = head;
      
      while(test.next!=null){
        while(test.data == test.next.data){
            if(test.next.next!=null){
                test.next = test.next.next;   
            }
            else {
                test.next = null;
                break;
            }
        }    
        if(test.next == null){
            break;
        }
        test = test.next;
      }
      return head;
      
        
    }

    public static  Node insert(Node head,int data)
    {
        Node p=new Node(data);			
        if(head==null)
            head=p;
        else if(head.next==null)
            head.next=p;
        else
        {
            Node start=head;
            while(start.next!=null)
                start=start.next;
            start.next=p;

        }
        return head;
    }
    public static void display(Node head)
        {
              Node start=head;
              while(start!=null)
              {
                  System.out.print(start.data+" ");
                  start=start.next;
              }
        }
        public static void main(String args[])
        {
              Scanner sc=new Scanner(System.in);
              Node head=null;
              int T=sc.nextInt();
              while(T-->0){
                  int ele=sc.nextInt();
                  head=insert(head,ele);
              }
              head=removeDuplicates(head);
              display(head);

       }
    }
{{</ highlight >}}


- 백준 부분 문제 해결 

    - https://www.acmicpc.net/problem/17225

    - 우선순위 큐를 이용해서 원하는 대로 정렬 후 문제를 해결함 

```java
import java.util.*;
import java.io.*;

public class test{

     public static void main(String []args){
        
        try{
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); 
            
            String[] test = br.readLine().split(" ");
            
            int sangmin = Integer.parseInt(test[0]);
            
            int jisu = Integer.parseInt(test[1]);
            
            int client = Integer.parseInt(test[2]);
            
            PriorityQueue<String> queue1 = new PriorityQueue<String>((x,y)->{

                int val1 = Integer.parseInt(x.split("-")[0]);
                int val2 = Integer.parseInt((y.split("-")[0]));

                if(val1 == val2){
                    return x.split("-")[1].compareTo(y.split("-")[1]);
                }
                return val1-val2;

            });
            int sangmincount = 0;
            int jisucount = 0;
            for(int i = 0 ; i < client;i++){
                
                String[] check = br.readLine().split(" ");
                
                int ordertime = Integer.parseInt(check[0]);
                
                String color = check[1];

                int parentCount = Integer.parseInt(check[2]);
                
                // System.out.println(ordertime + "," +color + "," + parentCount);
                
                if(color.equals("B")){
                    queue1.add(ordertime+"-B");
                    sangmincount++;
                    for(int j = 0 ; j < parentCount-1 ; j++){
                        ordertime = ordertime+sangmin;
                        queue1.add(ordertime+"-B");
                        sangmincount++;
                    }
                }
                else if(color.equals("R")){
                    queue1.add(ordertime+"-R");
                    jisucount++;
                    for(int j = 0 ; j < parentCount-1 ; j++){
                        ordertime = ordertime+jisu;
                        queue1.add(ordertime+"-R");
                        jisucount++;
                    }
                }
            
                // if(color.equals("B")){
                //     queue.add();
                // }
                // else if(color.equals("R")){
                    
                // }
                
            }
            br.close();
            
            
            int count = 1;
            // System.out.println(sangmincount+" " +jisuconut);
            ArrayList<Integer> arr1 = new ArrayList<Integer>();
            ArrayList<Integer> arr2 = new ArrayList<Integer>();
            while(queue1.size() != 0){
                String checking = queue1.poll();
                if(checking.contains("B")){
                    arr1.add(count);
                }
                else {
                    arr2.add(count);
                }
                count++;
                
            }
            System.out.println(sangmincount);
            for(int k : arr1){
                System.out.print(k+" ");
            }
            System.out.println();
            System.out.println(jisucount);
            for(int k : arr2){
                System.out.print(k+" ");
            }
        
        
        }
        catch(IOException e){
            
        }

     }
}
```


