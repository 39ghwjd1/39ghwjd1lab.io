---
title: SSAFY Problem Solving 12
subtitle: 
date: 2021-01-18
tags: ["code"]
type: post
---

알고리즘 문제 풀이 12일차 및 JS 개념 정리 

- Tree-coding repo에 관련 정보 정리해서 넣을 듯 

    - 자주 읽어보자 

<!--more-->

- Hackerrank 30 Days of Code 

    - https://www.hackerrank.com/challenges/30-bitwise-and/problem

    - bit 연산의 결과의 우선순위를 따져 출력하는 알고리즘 문제 

    - js 문법인 object.entries, 기본 라이브러리 사용법을 계속 까먹음 

        - 반복해서 연습하자 

    - https://developer.mozilla.org/ko/docs/Web/JavaScript/Reference/Global_Objects/Object/entries

{{< highlight javascript "linenos=inline">}}

'use strict';

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', _ => {
    inputString = inputString.replace(/\s*$/, '')
        .split('\n')
        .map(str => str.replace(/\s*$/, ''));

    main();
});

function readLine() {
    return inputString[currentLine++];
}



function main() {
    const t = parseInt(readLine(), 10);
    
    for (let tItr = 0; tItr < t; tItr++) {
        const nk = readLine().split(' ');

        const n = parseInt(nk[0], 10);

        const k = parseInt(nk[1], 10);
        
        let arr = [];
        
        let obj = {};
        
        for(let i = 1 ; i <= n ;i++){
            arr.push(i);
        }
        for(let i = 0 ; i < n-1; i++){
            for(let j = i+1 ; j < n; j++){
               let check = arr[i] & arr[j];
            //    console.log(arr[i], arr[j], check)
               if(obj[check]===undefined){
                   obj[check] = 1;
               }
               else {
                   obj[check]++; 
               }
            }
        }
                // console.log(obj)

        let result = 0;
        let resultkey = 0;
        // console.log(Object.entries(obj))
        for(const [key, value] of Object.entries(obj)){
            // console.log(key, value, result,k )
            if(key < k){
                //console.log("-------")
                resultkey = parseInt(key);
                result = value;
            }
        }
        console.log(resultkey);
        
        
    }
}
{{</ highlight >}}

- 백준 문제 2개 풀이 

    - 음.. 근데 아직 풀 수 있는 수준의 난이도가 낮다고 생각 ㅠㅠ

    - https://www.acmicpc.net/problem/14916

    - https://www.acmicpc.net/problem/1439

- 1439 번 문제 
```java 
import java.util.*;
import java.util.regex.*;
public class Main{

     public static void main(String []args){
        Scanner input = new Scanner(System.in);
        
        String test = input.next(); 
        Pattern p = Pattern.compile("1+");
        Matcher m = p.matcher(test);
        int count = 0; 
        int count1 = 0;
        
        while(m.find()){
            count1++;
            // System.out.println(m.group());
        }
        
        Pattern p1 = Pattern.compile("0+");
        Matcher m1 = p1.matcher(test);
        
        while(m1.find()){
            count++;
            // System.out.println(m1.group());
        }
        if(count1 < count){
            System.out.println(count1);
        }
        else {
            System.out.println(count);
        }
        
        
        // String[] a = test.split("1+");
        
        // System.out.println(test.length - a.length);
     }
}

```

- 14916번 문제 풀이 
```java 
import java.util.*;
public class Main{

     public static void main(String []args){
        Scanner input = new Scanner(System.in);
        
        int a = input.nextInt();
        
        int b = 0; 
        int c = 0; 
        
        // 5 * b + 2 * c = a;
        
        int d = a / 5;
        
        boolean no = false;
        for(int i = d ; i >= 0 ; i--){
            
            int check = (a - (5 * i));
            
            if(check % 2 == 0){
                
                b = i;
                c = check / 2;
                break;
            }
            if(i == 0 && check % 2 !=0)
            {
                no = true;
            }
            
            // int c = (a - (5 * i)) / 2;
        }
        
        if(no){
            System.out.println(-1);
        }else {
            System.out.println(b+c);    
        }
        
         
        
        
     }
}
```