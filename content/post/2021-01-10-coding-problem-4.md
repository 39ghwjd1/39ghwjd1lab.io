---
title: SSAFY Problem Solving 4 
subtitle: 
date: 2021-01-10
tags: ["code"]
type: post
---

알고리즘 문제 풀이 4일차 

<!--more-->

- Binary Search Tree 기본 

    - Hackerrank 30 Days Of Code 

    - 기본 재귀 문제로 풀 수 있는 메소드 구현 문제 

    - https://www.hackerrank.com/challenges/30-binary-search-trees/problem

{{< highlight java "linenos=inline">}}

public static int getHeight(Node root){
      //Write your code here
    
        int rootheight = 1;
        // System.out.println("-----"+root.data);
        if(root == null){
            return -1;
        }

        return rootheight + (Math.max(getHeight(root.left) , getHeight(root.right)));
}
{{</ highlight >}}
