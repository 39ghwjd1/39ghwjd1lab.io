---
title: SSAFY Problem Solving 8 
subtitle: 
date: 2021-01-14
tags: ["code"]
type: post
---

알고리즘 문제 풀이 8일차 

<!--more-->

- Hackerrank 30 Days of Code 

    - Day 26 nested Loop 

        - 조건에 따른 여러 경우를 생각하면 됨 

    - https://www.hackerrank.com/challenges/30-nested-logic/problem



{{< highlight java "linenos=inline">}}
function processData(input) {
    //Enter your code here
    let test1 = input.split("\n");
    
    let actual = test1[0].split(" ");
    let expected = test1[1].split(" ");
    
    let fee = 0;
    // 1
    if(parseInt(actual[2]) < parseInt(expected[2])){
        console.log(fee);
    }
    else {
        if(parseInt(actual[1]) === parseInt(expected[1])
            && parseInt(actual[2]) === parseInt(expected[2])
        ){
            if(parseInt(actual[0]) <= parseInt(expected[0])){
                console.log(fee);
            }
            else {
                let fees = parseInt(actual[0]) - parseInt(expected[0]);
                console.log(fees*15);
            }
        }
        else {
            if(parseInt(actual[2]) === parseInt(expected[2])){
                if(parseInt(actual[1]) > parseInt(expected[1])){
                    let fees = parseInt(actual[1]) - parseInt(expected[1]);
                    
                    console.log(fees * 500)
                }
                else {
                    if(parseInt(actual[1]) < parseInt(expected[1])){
                        console.log(0);
                    }
                    else if(parseInt(actual[1]) === parseInt(expected[1])){
                        if(parseInt(actual[0]) > parseInt(expected[0])){
                            console.log(15 * (parseInt(actual[0]) -parseInt(expected[0])));
                        }
                        else {
                            console.log(0)
                        }
                    }
                    else {
                       console.log(500*(parseInt(actual[1]) - parseInt(expected[1]))) 
                    }
                    
                }
                
            }
            else {
               if(parseInt(actual[2]) > parseInt(expected[2])){
                   console.log(10000);
               }
               else if(parseInt(actual[1]) <= parseInt(expected[1])){
                   
                   if(parseInt(actual[0]) <= parseInt(expected[0])){
                       console.log(0)
                   }
                   else {
                    let fees = parseInt(actual[0]) - parseInt(expected[0]);
                    console.log(fees*15)   
                   }
               }
            }
        }
    }
    // console.log(test1);
      
} 
{{</ highlight >}}

- baekjoon 1337

    - https://www.acmicpc.net/problem/1337

    - 동적 배열이 없어서 arraylist로 구현

    - arrays에서 특정 원소 찾기 

        - https://docs.oracle.com/javase/8/docs/api/java/util/Arrays.html#binarySearch-int:A-int-

        - https://jjeong.tistory.com/1250

```java
import java.util.*;

public class Main{

     public static void main(String []args){
        
        Scanner input = new Scanner(System.in);
        
        int val = input.nextInt();
        int[] arr = new int[val];
        for(int i = 0; i < val ; i++){
            arr[i] = input.nextInt();
        }
        
        Arrays.sort(arr);
        int size = 5; 
        for(int i = 0 ; i < val; i++)
        {
            
            ArrayList<Integer> arraylist= new ArrayList<Integer>();
            
            for(int j = 0 ; j < 5 ; j++){
                
                int k = Arrays.binarySearch(arr, arr[i]+j);
                if(k >= 0){
                     
                }
                else {
                    arraylist.add(arr[i]+j);
                }
            }
            
            if(size > arraylist.size()){
                size = arraylist.size(); 
            }
            
        }
        
        System.out.println(size);
     }
}
```