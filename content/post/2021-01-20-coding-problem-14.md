---
title: SSAFY Problem Solving 14
subtitle: 
date: 2021-01-20
tags: ["code"]
type: post
---

알고리즘 문제 풀이 14일차   

- Tree-coding repo에 관련 정보 정리해서 넣을 듯 => 하나 공부하면서 직접 번역해서 추가함 
    - 관련 링크들을 찾아 더 딥하게 들어가야 함 

- 조금씩 코딩 문제가 버거워 진다
    - 알고리즘 책, 인강을 보면서 해결해 볼 생각이다
    - 과연 내것으로 만들 수 있을까

<!--more-->

- 백준 1475

    - 9와 6이 나오는 횟수를 한 쪽으로 몰아서 생각하였습니다. 
         - 서로 대체할 수 있기 때문 
    
    - IDE에서 했던 것처럼 package 를 위에 추가하면 런타임 에러가 백준에서 나는 것을 확인
        - package 작성은 백준에선 안 하는 것이 좋을 거 같습니다.
    
    - 무조건 class 이름은 Main으로 작성해야 합니다.

{{< highlight java "linenos=inline">}}
import java.util.*;

public class Main{

     public static void main(String []args){
        Scanner input = new Scanner(System.in);
        
       String test = input.next();
       
       String[] test1 = test.split("");
       int count = 0;
       int[] test2 = new int[10]; 
       for(String k : test1){
           
           int check = Integer.parseInt(k);
           
           test2[check]++;
        //   System.out.println();
       }
       
       test2[6] += test2[9];
       
       test2[9] = 0;
       
       test2[6] = (test2[6] / 2) + (test2[6] % 2);
       
       int max = Integer.MIN_VALUE; 
       
       for(int k1: test2){
           if(max < k1){
               max = k1;
           }
       }
       System.out.println(max);
      
     }
}
{{</ highlight >}}
```