---
title: SSAFY Problem Solving 5 
subtitle: 
date: 2021-01-11
tags: ["code"]
type: post
---

알고리즘 문제 풀이 5일차 

<!--more-->

- Binary Search Tree의 Level Traversal

    - Hackerrank 30 Days Of Code 

    - 처음에는 재귀로 풀어야 한다고 생각을 했지만, 너무 연산이 많을 거 같고, 직접 해보니 중복 출력되는 부분이 존재 

    - Breath First Search 의 방법과 같이 queue를 이용해서 root부터 넣고 뺀 다음 left, right가 있는지 체크하고 다시 queue에 넣고 빼서 data를 출력하는 방식으로 구현하면 됨 

    - if - else if를 내부에서 사용하게 되면 오른쪽 자식부분은 거치지 않음 => 나의 실수 주의!
    
    - https://java2blog.com/binary-tree-level-order-traversal-java/

{{< highlight java "linenos=inline">}}

static void levelOrder(Node root){
      //Write your code here
      
      Queue<Node> queue = new LinkedList<Node>();
      queue.add(root);
      while(!queue.isEmpty()){
          Node node = queue.poll();
          System.out.print(node.data +" ");
          if(node.left!=null){
              queue.add(node.left);
          }
          if(node.right!=null){
              queue.add(node.right);
          }
      }      
}
{{</ highlight >}}


- Java Prime checker

    - https://www.hackerrank.com/challenges/prime-checker/problem

    - 일반적인 소수 체크 문제하고는 다름 

    - 오버로딩을 금지한 문제 => 어떻게 하나의 메소드에서 다양한 매개변수를 통해 호출하지?

    - int...parameter => varargs를 이용한다. 

    - https://docs.oracle.com/javase/1.5.0/docs/guide/language/varargs.html

    - import static 를 이용해서 풀기도 해야 함 

    - https://offbyone.tistory.com/283

{{< highlight java "linenos=inline">}}

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;
import java.lang.reflect.*;
import static java.lang.System.*;
class Prime{
    public void checkPrime(int... arr){
        ArrayList<Integer> arraylist = new ArrayList<Integer>();
        for(int a : arr){
            boolean test = true;
            if(a == 1){
                continue;
            }
            for(int i = 2; i <= Math.sqrt(a); i++){
                if(a % i == 0){
                    test = false;
                }
            }            
            if(test){
                arraylist.add(a);
            }
            
        }
        
        if(arraylist.size() == 0){
            System.out.println();
        }
        else{
            for(int k : arraylist){
                System.out.print(k + " ");
            }
            System.out.println();
        }

    }
    
}



public class Solution {

	public static void main(String[] args) {
		try{
		BufferedReader br=new BufferedReader(new InputStreamReader(in));
		int n1=Integer.parseInt(br.readLine());
		int n2=Integer.parseInt(br.readLine());
		int n3=Integer.parseInt(br.readLine());
		int n4=Integer.parseInt(br.readLine());
		int n5=Integer.parseInt(br.readLine());
		Prime ob=new Prime();
		ob.checkPrime(n1);
		ob.checkPrime(n1,n2);
		ob.checkPrime(n1,n2,n3);
		ob.checkPrime(n1,n2,n3,n4,n5);	
		Method[] methods=Prime.class.getDeclaredMethods();
		Set<String> set=new HashSet<>();
		boolean overload=false;
		for(int i=0;i<methods.length;i++)
		{
			if(set.contains(methods[i].getName()))
			{
				overload=true;
				break;
			}
			set.add(methods[i].getName());
			
		}
		if(overload)
		{
			throw new Exception("Overloading not allowed");
		}
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
	}
	
}
  
{{</ highlight >}}



