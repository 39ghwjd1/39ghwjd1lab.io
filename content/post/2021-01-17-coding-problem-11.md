---
title: SSAFY Problem Solving 11
subtitle: 
date: 2021-01-17
tags: ["code"]
type: post
---

알고리즘 문제 풀이 11일차 

<!--more-->

- Hackerrank 30 Days of Code 

    - https://www.hackerrank.com/challenges/30-bitwise-and/problem

    - bit 연산의 결과의 우선순위를 따져 출력하는 알고리즘 문제 

    - js 문법인 object.entries, 기본 라이브러리 사용법을 계속 까먹음 

        - 반복해서 연습하자 

    - https://developer.mozilla.org/ko/docs/Web/JavaScript/Reference/Global_Objects/Object/entries

{{< highlight javascript "linenos=inline">}}

'use strict';

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', _ => {
    inputString = inputString.replace(/\s*$/, '')
        .split('\n')
        .map(str => str.replace(/\s*$/, ''));

    main();
});

function readLine() {
    return inputString[currentLine++];
}



function main() {
    const t = parseInt(readLine(), 10);
    
    for (let tItr = 0; tItr < t; tItr++) {
        const nk = readLine().split(' ');

        const n = parseInt(nk[0], 10);

        const k = parseInt(nk[1], 10);
        
        let arr = [];
        
        let obj = {};
        
        for(let i = 1 ; i <= n ;i++){
            arr.push(i);
        }
        for(let i = 0 ; i < n-1; i++){
            for(let j = i+1 ; j < n; j++){
               let check = arr[i] & arr[j];
            //    console.log(arr[i], arr[j], check)
               if(obj[check]===undefined){
                   obj[check] = 1;
               }
               else {
                   obj[check]++; 
               }
            }
        }
                // console.log(obj)

        let result = 0;
        let resultkey = 0;
        // console.log(Object.entries(obj))
        for(const [key, value] of Object.entries(obj)){
            // console.log(key, value, result,k )
            if(key < k){
                //console.log("-------")
                resultkey = parseInt(key);
                result = value;
            }
        }
        console.log(resultkey);
        
        
    }
}
{{</ highlight >}}

-