---
title: SSAFY Problem Solving 2
subtitle: 
date: 2021-01-08
tags: ["code"]
type: post
---

알고리즘 문제 풀이 2일차 

<!--more-->

- Bubble Sort 간단한 정렬 문제 

    - https://www.hackerrank.com/challenges/30-sorting/problem

    - https://programmers.co.kr/learn/courses/30/lessons/12944?language=javascript

    - reduce 함수를 이용해서 풀면 쉽게 가능 

```javascript
         let numSwap = 0;
    
    for(let i = 0 ; i < n ; i++){
        
        
        for(let j = 0 ; j < n-1; j++){
            if(a[j] > a[j+1]){
                let test = a[j];
                a[j] = a[j+1];
                a[j+1] = test;
                
                numSwap++;
            }
        }
        
        if(numSwap === 0){
            break;
        }
           
    }
    
    console.log(`Array is sorted in ${numSwap} swaps.`);
    console.log(`First Element: ${a[0]}`);
    console.log(`Last Element: ${a[a.length-1]}`);
```

- 추가 내용 

    - https://www.hackerrank.com/challenges/duplicate-word/problem

    - 이 Hackerrank에 문제가 있다는 것을 알아냄 

        - 주석을 This라고 적힌 부분에 추가하면 문제 답이 맞는데도 오류가 나는 경우가 생김 
            - 답을 그대로 하고 주석을 처리하면 오류가 발생!

        - 그리고 regex에서 비포획괄호 (?:)에 대해 공부함 

            - 단어 전체를 {}로 지정할 수 있게 해줌 

            - https://developer.mozilla.org/ko/docs/Web/JavaScript/Guide/%EC%A0%95%EA%B7%9C%EC%8B%9D
                - 이곳에 들어갔다가 이제 ?:로 검색해서 찾으면 쉽게 알 수 있음 

{{< highlight java "linenos=inline">}}
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DuplicateWords {

    public static void main(String[] args) {

        String regex = "\\b(\\w+)(\\s+\\1\\b)+";
        Pattern p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);

        Scanner in = new Scanner(System.in);
        int numSentences = Integer.parseInt(in.nextLine());
        
        while (numSentences-- > 0) {
            String input = in.nextLine();
            
            Matcher m = p.matcher(input);
            
            // Check for subsequences of input that match the compiled pattern
            while (m.find()) {
                // System.out.println(m.group()); -> This!
                input = input.replaceAll(m.group(),m.group(1));  
            }
            
            // Prints the modified sentence.
            System.out.println(input);
        }
        
        in.close();
    }
}
{{</ highlight >}}
